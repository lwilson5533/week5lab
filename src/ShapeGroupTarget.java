// --------------- imports ------------------------------
import javafx.scene.paint.Color;
import wheelsFX.users.Frame.AnimationTimer;
import wheelsFX.users.Frame.Animator;
import wheelsFX.users.Shapes.Ellipse;
import wheelsFX.users.Frame.Frame;
import wheelsFX.users.Shapes.ShapeGroup;

/**
 * Target.java:
 * <p>
 * Displays a simple archery target using multiple Wheels Shapes.
 * The entire target is built in a method, makeTarget.
 * <p>
 * makeTarget has position arguments.
 *
 * @author Professor Rossi
 * @author Laura Wilson
 */


public class ShapeGroupTarget extends ShapeGroup implements Animator

{
    private int direction = 1;

    //---------------- instance variables ------------------------------
    // local "constant" variables define the sizes of all circles
    private final int LEVEL_1_SIZE = 80;
    private final int LEVEL_2_SIZE = 50;
    private final int LEVEL_3_SIZE = 30;
    private final int LEVEL_4_SIZE = 20;

    // other local variables are used to references  Wheels objects
    // used to draw the target.
    protected Ellipse level1;
    protected Ellipse level2;
    protected Ellipse level3;
    protected Ellipse level4;
    // -----------------------------------------------------------------

    /**
     * Constructor for the TargetApp class.
     */
    public ShapeGroupTarget()
    {
        super();
        makeTarget( 0, 0 );
    }
    ////////////////////////////////////////////////////////////////////
    /**
     * 2 parameter constructor goes here:
     */
    // Write constructor with position parameters (2 ints)
    public ShapeGroupTarget( int x, int y )
    {
        super();
        this.makeTarget( x, y );
    }

    ////////////////////////////////////////////////////////////////////
    // -----------------------------------------------------------------

    // -----------------------------------------------------------------

    /**
     * move( int dx, int dy ).
     * move the location of the target by dx and dy
     * newx = oldx + dx
     * newy = oldy + dy
     * use Target's setLocation method to actually change the location
     *
     * @param dx int
     * @param dy int
     */
    public void move( int dx, int dy ) {
        /////////////////////////////////////////////////////////
        // move code here
        int x = super.getXLocation() + dx;
        int y = super.getYLocation() + dy;
        super.setLocation( x, y );
        ////////////////////////////////////////////////////////
    }

    // -----------------------------------------------------------------

    /**
     * makeTarget.
     * encapsulates all the Wheels components needed to draw a target.
     *
     * @param x int
     * @param y int
     */
    public void makeTarget( int x, int y ) {
        // create the level1 circle
        level1 = new Ellipse( x, y );
        level1.setSize( LEVEL_1_SIZE, LEVEL_1_SIZE );
        super.add( this.level1 );

        // create the next level4 circle
        level2 = new Ellipse( x, y );
        level2.setSize( LEVEL_2_SIZE, LEVEL_2_SIZE );
        level2.setColor( Color.BLUE );
        super.add( this.level2 );

        // create the next level4 circle
        level3 = new Ellipse( x, y );
        level3.setSize( LEVEL_3_SIZE, LEVEL_3_SIZE );
        level3.setColor( Color.CYAN );
        super.add( this.level3 );

        // create the level4 circle
        level4 = new Ellipse( x, y );
        level4.setColor( Color.BLACK );
        level4.setSize( LEVEL_4_SIZE, LEVEL_4_SIZE );
        super.add( this.level4 );
    }

    // -----------------------------------------------------------------
    @Override
    public void animate()
    {
        if (super.getXLocation() + 10 + (this.level1.getWidth()/2) >= Frame.FRAME_WIDTH)
        {
            this.direction *= -1;
        }
        else if (super.getXLocation() - (this.level1.getWidth()/2) == 0)
        {
            this.direction *= -1;
        }

        int x = super.getXLocation() + (10 * this.direction);
        super.setLocation(x, super.getYLocation());
    }
    /**
     * main program just invokes the class constructor.
     *
     * @param args String
     */
    public static void main( String[] args )
    {
        ShapeGroupTarget t1 = new ShapeGroupTarget();
        t1.setLocation( 200, 200 );

        AnimationTimer timer = new AnimationTimer(t1);

        Frame.createFrame();
    }



} //End of Class TargetApp
