import wheelsFX.users.Frame.AnimationTimer;
import wheelsFX.users.Frame.Frame;

/**
 * Week5Lab.java
 *
 * This class shows the demonstration
 * of the ShapeGroupTarget class animation and the
 * BlobGuy class animation
 *
 * @author Laura Wilson
 */

public class Week5Lab
{
    public static void main (String [] arg)
    {
        ShapeGroupTarget t1 = new ShapeGroupTarget();
        t1.setLocation( 200, 200 );

        AnimationTimer timer = new AnimationTimer(t1);

        BlobGuy b1 = new BlobGuy(100,100);

        AnimationTimer timer2 = new AnimationTimer(b1);

        Frame.createFrame();
    }
}
