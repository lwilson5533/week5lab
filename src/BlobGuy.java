//-------------imports
import javafx.scene.paint.Color;
import wheelsFX.users.Frame.Animator;
import wheelsFX.users.Frame.Frame;
import wheelsFX.users.Shapes.*;


/**
 * BlobGuy.java:
 *
 * Displays a simple body and head "Blob Guy"
 *
 * makeBlobGuy has position arguments.
 *
 * @author Laura Wilson
 */

public class BlobGuy extends ShapeGroup implements Animator
{
    //--------instance variables-------
    private Ellipse head;
    private Ellipse body;

    private int direction = 1;

    /**
     * default constructor for BlobGuy class
     */
    public BlobGuy ()
    {
        super();
        makeBlobGuy(0,0);
    }

    /**
     * constructor for BlobGuy class that creates
     * object at some:
     * @param x
     * @param y
     */
    public BlobGuy(int x, int y)
    {
        super();
        makeBlobGuy(x,y);
    }

    /**
     * method creates and builds blob guy using some:
     * @param x
     * @param y
     *
     * as center of body
     */
    public void makeBlobGuy(int x, int y)
    {
        body = new Ellipse(x, y);
        body.setSize(50, 50);
        body.setColor(Color.BLUE);
        super.add(this.body);

        head = new Ellipse(x,y-50);
        head.setSize(20,20);
        head.setColor(Color.PALEGREEN);
        super.add(this.head);
    }

    //--------------------------------------
    @Override
    public void animate()
    {
        if (super.getYLocation() + 5 + (this.body.getWidth()/2) >= Frame.FRAME_HEIGHT)
        {
            this.direction *= -1;
        }
        else if (this.head.getYLocation() - (this.head.getWidth()/2) == 0)
        {
            this.direction *= -1;
        }

        int y = super.getYLocation() + (5 * this.direction);
        super.setLocation(super.getXLocation(),y);

        if (super.getXLocation() + 5 + (this.body.getWidth()/2) >= Frame.FRAME_WIDTH)
        {
            this.direction *= -1;
        }
        else if (this.body.getXLocation() - (this.body.getWidth()/2) == 0)
        {
            this.direction *= -1;
        }

        int x = super.getXLocation() + (5 * this.direction);
        super.setLocation(x,super.getYLocation());
    }
}
